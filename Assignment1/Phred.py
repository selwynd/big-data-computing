

class Get_phred:

    def get_score(input, start,stop, q):
        '''Given a start byte will find read scores and adds the sums of the scores to lists.
        Will stop and return if the current byte exceeds the stop byte.'''
        flag = True

        sum = []
        amount = []
        with open(input) as fastq:
            fastq.seek(start)
            while flag:
                if fastq.readline() == "+\n":
                    scores = fastq.readline().rstrip()

                    for i, score in enumerate(scores):

                        score = int(ord(score)-33)
                        if len(sum) < (i + 1):
                            sum.append(0)
                            amount.append(0)

                        sum[i] += score
                        amount[i] += 1

                    if fastq.tell() >= stop:
                        q.put([sum, amount])
                        print("Process with start byte {} and stop byte {} has finished".format(start,stop))
                        flag = False
                        return
                else:
                    pass
            return