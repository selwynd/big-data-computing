import os, time
from multiprocessing import Queue, Process, Pipe

from Input_Output import Argument_parse
from Phred import Get_phred


'''
Author      Selwyn Dijkstra
Titel       fastq_scorer
Function    Give the average phred score of a fastqfile per nucleotide position

'''

class data_flow:
    def __init__(self):
        pass
    def make_process(self, input, cores):
        '''Takes the amount of threads and divides the input file into n amount of bytes.
        Then creates a process with the given start byte returns 2 lists one with the sums of all scores and one with
        the amount of scores found for each position.'''
        worker_list = []
        q = Queue()
        stats = os.stat(input)
        piece_size = int((stats.st_size) / int(cores))

        for worker in range(0, int(cores)):
            p = Process(target=Get_phred.get_score, args=((input), worker * piece_size, (worker * piece_size) + piece_size, q))
            worker_list.append(p)
            p.start()
            print("Starting new process")

        all_sums = []
        all_amounts = []

        for worker in worker_list:
            worker.join()
            sums, amounts = q.get()
           #sums, amounts = master_conn.recv() #Here you get the results using the pipe function sending results from the worker to the master
            all_sums.append(sums)
            all_amounts.append(amounts)


        all_amounts = [sum(x) for x in zip(*all_amounts)]
        all_sums = [sum(x) for x in zip(*all_sums)]

        return [all_sums, all_amounts]

def main():
    in_out = Argument_parse()
    args = in_out.Parser()

    d = data_flow

    timer = time.time()
    results = d.make_process(d, args.fastq, args.n)
    timed = "Time taken is {} Seconds".format(round(time.time() - timer))

    print(timed)

    if args.o:
        in_out.write_csv(results, args.o)

if __name__ == '__main__':
    main()