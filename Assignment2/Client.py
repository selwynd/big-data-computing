from multiprocessing import Manager, Process, managers
from Input_Output import Argument_parse
import time, sys, os


class Worker:
    def __init__(self, args):
        self.input = args.fastq
        self.cores = args.n
        args.c = (args.c[0].split(','))
        self.ip = args.c[3]
        self.client = args.c[0]
        self.port = int(args.c[1])
        self.authkey = args.c[2]
        self.runclient()


    def make_processes(self, job_q, result_q, input, cores):
        '''Making multiple processes for this client'''
        worker_list = []

        for worker in range(0, int(cores)):
            p = Process(target=self.get_score, args=(input, job_q, result_q))
            worker_list.append(p)
            p.start()
            # print("Starting new process")

        for worker in worker_list:
            worker.join()
        return

    def get_score(self, input, job_q, result_q):
        '''Given a start byte will find read scores and adds the sums of the scores to lists.
        Will stop and return if the current byte exceeds the stop byte.'''
        flag = False
        sum = []
        amount = []
        while not job_q.empty():
            with open(input) as fastq:
                try:
                    if flag == False and not job_q.empty():
                        try:
                            start, stop = job_q.get()
                            os.system("echo {} {} {}".format(start, stop, self.client))
                        except EOFError:
                            return 0
                        fastq.seek(start)
                        flag = True
                    else:
                        return 0

                    while flag:
                        if fastq.readline() == "+\n":
                            scores = fastq.readline().rstrip()
                            for i, score in enumerate(scores):
                                score = int(ord(score)-33)
                                if len(sum) < (i + 1):
                                    sum.append(0)
                                    amount.append(0)

                                sum[i] += score
                                amount[i] += 1

                            if fastq.tell() >= stop:
                                result_q.put([sum, amount])
                                #print("Process at {} with start byte {} and stop byte {} has finished".format(self.client, start,stop))
                                if job_q.qsize() == 0:
                                    return
                                flag = False

                        else:
                            pass

                except job_q.empty():
                    return 0
        return 0


    def client_manager(self, ip, port, authkey):

        class ServerQueueManager(managers.SyncManager):
            pass

        ServerQueueManager.register('get_job_q')
        ServerQueueManager.register('get_result_q')
        manager = ServerQueueManager(address=(ip, port), authkey=b'testtest')

        tries = 1
        while True:
            try:
                if tries == 2:
                    os.system("echo 'No manager found, shutting down'")
                    sys.exit()
                os.system("echo Connection try attempt {}".format(tries))
                manager.connect()
                break
            except ConnectionRefusedError:
                time.sleep(2)
                tries += 1

        os.system('echo Client connected to {}:{}'.format(self.ip, self.port))
        return manager

    def runclient(self):
        manager = self.client_manager(self.ip, self.port, self.authkey)
        job_q = manager.get_job_q()
        result_q = manager.get_result_q()
        self.make_processes(job_q, result_q, self.input, self.cores)
        sys.exit()
        return

def main():
    in_out = Argument_parse()
    args = in_out.Parser()

    args.fastq = "".join(args.fastq)
    worker = Worker(args)
    return

if __name__ == '__main__':
    main()
