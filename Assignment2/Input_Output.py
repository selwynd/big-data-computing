import argparse, os, csv

class Argument_parse:
    def __init__(self):
        pass

    def Parser(self):
        '''Parses the command line arguments. Default options are used if no parameter is set.'''
        parser = argparse.ArgumentParser()
        parser.add_argument("fastq", nargs='+', help="Give the input file in fastq format")
        parser.add_argument("-n", "--n", help="Amount of threads used for multiprocessing", default=4)
        parser.add_argument("-o", "--o", help="Name for output file in csv format.", default=None)
        parser.add_argument("-c", "--c", nargs='+',help="All connectifity settings only for client", default=None)
        return parser.parse_args()

    def write_csv(self, results, file_out, x):
        '''Write the lists of results per entry to a given filename as output.'''
        sums, amounts = results

        if not os.path.exists("output/"):
            os.mkdir("output/")
        else:
            pass

        with open("output/{}-{}".format(x,file_out), 'w') as output:
            writer = csv.writer(output)
            writer.writerows([["Position", "Average Score"]])
            for i, sum in enumerate(sums):

                # line = "Position "+str(i)+" has average quality score of "+ str(sum/amounts[i]) + "\n"
                writer.writerows([[str(i), str(sum/amounts[i])]])
        os.system("echo 'Output has been generated'")
        output.close()
