import os, time, sys
from multiprocessing import Queue, Process, Pipe, managers
import random

from Input_Output import Argument_parse
from Client import Worker
import subprocess


'''
Author      Selwyn Dijkstra
Titel       fastq_scorer
Function    Give the average phred score of a fastqfile per nucleotide position

'''

class Data_flow:
    def __init__(self, args):
        self.clients = ["bin305","bin306","bin307", "bin210"]
        # self.master = 'bin210'
        self.port_list = range(15000, 20000)
        self.args = args
        self.results = self.master_server(args.fastq)




    def start_master(self,master_ip, port, authkey):
        job_q = Queue()
        result_q = Queue()

        class JobQueueManager(managers.SyncManager):
            pass

        JobQueueManager.register('get_job_q', callable=lambda: job_q)
        JobQueueManager.register('get_result_q', callable=lambda: result_q)

        manager = JobQueueManager(address=(master_ip, port), authkey=authkey)
        manager.start()
        print('Master Server started at port %s' % port)
        return manager

        pass

    def master_server(self, fastq):
        port = random.choice(self.port_list) #Take a random port
        master_ip = "bin210"
        manager = self.start_master(master_ip, port, b'testtest') # MASTER MANAGER
        shared_job_q = manager.get_job_q()
        shared_result_q = manager.get_result_q()

        stats = os.stat(fastq)
        piece_amount = 1000
        piece_size = int((stats.st_size) / piece_amount)

        start = 0
        stop = piece_size

        for i in range(0, piece_amount):
            start += piece_size
            stop += piece_size
            shared_job_q.put([piece_size*i, (piece_size*i)+piece_size])

        self.create_clients(master_ip, port, b'testtest')

        all_sums = []
        all_amounts = []

        results = 0

        print("Waiting on results")

        while results <= piece_amount:
            sums, amounts = shared_result_q.get()
            all_sums.append(sums)
            all_amounts.append(amounts)
            results += 1
            if results == piece_amount:
                break

        print("Results received")

        all_amounts = [sum(x) for x in zip(*all_amounts)]
        all_sums = [sum(x) for x in zip(*all_sums)]

        time.sleep(2)
        manager.shutdown()

        return [all_sums, all_amounts]



    def create_clients(self,master_ip, port, auth):
        string = []
        client_path = os.path.abspath("Client.py")
        for client in self.clients:
            # print(os.path.abspath("Client.py"))
            # print(self.args.fastq)
            # subprocess.Popen("python3 /homes/sdijkstra/PycharmProjects/big-data-computing/Assignment2/Client.py {} -c '{},{},{}' -n {}".format(self.args.fastq, client,port,auth, self.args.n))
            # os.system("ssh {} python3 /homes/sdijkstra/PycharmProjects/big-data-computing/Assignment2/Client.py {} -c '{},{},{}' -n {}".format(client,self.args.fastq, client,port,auth, self.args.n)) # Connect to client target terminal
            #~ os.system("tmux new -s phred_client")
            c = "{},{},{},{}".format(client,port,auth,master_ip)
            subprocess.Popen(["ssh", client,"python3", client_path, self.args.fastq, "-c", c, "-n", self.args.n])
            # t = ["ssh", client,"python3", "/homes/sdijkstra/PycharmProjects/big-data-computing/Assignment2/Client.py", self.args.fastq, "-c", c, "-n", self.args.n]
            # string.append(t)
            # print(client)
            # ~ os.system("python3 /homes/sdijkstra/PycharmProjects/big-data-computing/Assignment2/Client.py")
            # string.append("ssh {} python3 /homes/sdijkstra/PycharmProjects/big-data-computing/Assignment2/Client.py {} -c '{},{},{}' -n {}".format(client, self.args.fastq, client,port,auth, self.args.n))
        # t = "".join((string[1]," & ", string[0] ," & ",string[2], " & ", string[3]))
        # subprocess.Popen(["echo", "test"])
        # print(string)
        # print(t)
        # proc = [subprocess.Popen(i) for i in string]
        # os.system(t)
        return


def main():
    in_out = Argument_parse()
    args = in_out.Parser()
    timer = time.time()

    for fastq in args.fastq:
        fastq = "".join(fastq)
        args.fastq = fastq
        data = Data_flow(args)
        results = data.results
        if args.o:
            w = "".join(args.fastq)
            t = w.split('/')
            x = t[-1]
            name, extension = x.split(".")
            in_out.write_csv(results, args.o, name)


    timed = "Time taken is {} Seconds".format(time.time() - timer)
    print(timed)



    sys.exit()

if __name__ == '__main__':
    main()
