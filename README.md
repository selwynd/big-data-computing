## Big Data Computing

Year 2018-2019

By Selwyn Dijkstra

BFV-3

## Assignment 1

This script takes a fastq file as input and will determine the average score per base in all reads. 

To call the function use : python3 <script.py> <input.fastq> <n amount of cores> <o output file>

Optionel parameters are:  

-n for determining the amount of threads used by the program (default is 4 threads)

-o for writing the results to an output file. (default is None) 

## Assignment 2

This script takes a fastq file as input and will determine the average score per base of all read using multiple pc's.
In order to run master.py you need to connect to the master manager on bin210. 
Multiple fastq files can be used each one will generate a seperate results csv

Commands to use:

1. ssh bin210
2. python3 <master.py> <fastq_input> <fastq_file> <-n number of threads> <-o output csv name>

Optional parameters are:

-n for determining the amount of threads used by each of the client pc's (default is 4 threads)

-o for writing the results to an output file. (default is None) 

-c is only used by the master.py to give each client the proper port and ip.